//Admin tool for managing settings and permissions for a particular guild
import {Extension} from './extension';
import {OmegaDB} from "./omegaDB";
import {Guild} from "./guild";
import Discord = require('discord.js');
import {mapDir, createEmbed} from './omegaToolkit';

const namespace: string = "admin"; //Declares the extensions namespace
const events: string[] = []; //Declares what events the extension wants

const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {
    permission: {
        commandUsage: "", 
        commandInfo: ""
    },
    botCommand: {
        commandUsage: "",
        commandInfo: ""
    },
    extensions: {
        commandUsage: "",
        commandInfo: ""
    },
    commands: {
        commandUsage: "",
        commandInfo: ""
    }
}; 

//Initializes a new extension class and returns it to the caller
function initExtension(guild: Guild, database: OmegaDB) {
    return new OmegaAdmin(guild, database, commands);
}

class OmegaAdmin extends Extension {

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        super(guild, database, commands);
    }

    //List commands
    listCommands() {
        //This needs to show complete command structure for this extension
        return [this.exposedCommands];
    }
    //Handles and resolves commands
    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        let command: string = cmd[0];
        let args: string[] = cmd.slice(1);

        switch (command) {
            case "permissions":
                this.permissions(args, message);
                break;
            case "botCommand":
                this.botCommand(args, message);
                break;
            case "extensions":
                this.extensions(args, message);
                break;
            case "commands":
                this.commands(args, message);
                break;
            default:
                this.guild.reportError(message, message.member.user, "There are no command in admin extension matching: " + command, namespace);
                break;
        }
        return;
    }

    //Handles permission changes
    //TODO!!! Write code that informs caller of usefull information regarding what was done.
    private permissions(cmd: string[], message: Discord.Message) {
        let subCommand: string = cmd[0];
        let args: string[] = cmd.slice(1);

        let namespace: string = args[0];
        let command: string = "#namespace";
        if (!args[1].includes('@')) command = args[1];

        let response: Discord.RichEmbed;
        let users: Discord.User[];
        let roles: Discord.Role[];

        if (this.guild.guildPermissions[namespace] === undefined || (this.guild.guildPermissions[namespace][command] === undefined && command !== "#namespace")) return; //Inform user the supplied namespace and/or command is not viable

        switch(subCommand) {
            case "setLevel":
                let permissionLevel: number = parseInt(args[2]);
                if (permissionLevel === NaN) {
                    this.guild.reportError(message, message.member.user, "There was no permission resolvable (number) in the command", namespace);
                    return;
                }
                
                this.guild.guildPermissions[namespace][command].permissionLevel = permissionLevel;
                this.database.updatePermissionObject(this.guild.guildPermissions);

                response = createEmbed("The permission level for the command: " + command + " on the namespace: " + namespace + " has been set to: " + args[2]);
                break;
            case "setUserOverride":
                users = message.mentions.users.array();
                if (users.length === 0) {
                    this.guild.reportError(message, message.member.user, "There were no users mentioned in the command", namespace);
                    return;
                }

                var user: Discord.User;
                if (command !== "#namespace") {
                    for (var i in users) {
                        user = users[i];

                        if (!this.guild.guildPermissions[namespace][command].permissionUserOverrides.includes(user.id)) {
                            this.guild.guildPermissions[namespace][command].permissionUserOverrides.push(user.id)
                        } else {
                            console.log("User already has permissions"); //Maybe let cmd source know?
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);

                    response = createEmbed("Permissions for the command: " + command + " on the namespace: " + namespace + " have been set for the specified users"); 
                } else {
                    let commands = this.guild.guildPermissions[namespace];
                    for (var aCommand in commands) {
                        for (var i in users) {
                            user = users[i];
    
                            if (!this.guild.guildPermissions[namespace][aCommand].permissionUserOverrides.includes(user.id)) {
                                this.guild.guildPermissions[namespace][aCommand].permissionUserOverrides.push(user.id)
                            } else {
                                console.log("User already has permissions"); //Maybe let cmd source know?
                            }
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for all commands on the namespace: " + namespace + " have been set for the specified users");
                }      
                message.channel.send(response);  
                break;
            case "setRoleOverride":
                roles = message.mentions.roles.array();
                if (roles.length === 0) {
                    this.guild.reportError(message, message.member.user, "There were no roles mentioned in the command", namespace);
                    return;
                }

                var role: Discord.Role;
                if (command !== "#namespace") {
                    for (var i in roles) {
                        role = roles[i];
                        if (!this.guild.guildPermissions[namespace][command].permissionRoleOverrides.includes(role.id)) {
                            this.guild.guildPermissions[namespace][command].permissionRoleOverrides.push(role.id)
                        } else {
                            console.log("Role already has permissions"); //Maybe let cmd source know?
                        }
                    }
                    
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for the command: " + command + " on the namespace: " + namespace + " have been set for the specified roles");
                } else {
                    let commands = this.guild.guildPermissions[namespace];
                    for (var aCommand in commands) {
                        for (var i in roles) {
                            role = roles[i];
                            if (!this.guild.guildPermissions[namespace][aCommand].permissionRoleOverrides.includes(role.id)) {
                                this.guild.guildPermissions[namespace][aCommand].permissionRoleOverrides.push(role.id)
                            } else {
                                console.log("Role already has permissions"); //Maybe let cmd source know?
                            }
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for all commands on the namespace: " + namespace + " have been set for the specified roles");
                }

                message.channel.send(response);
                break;
            case "removeUserOverride":
                users = message.mentions.users.array();
                if (users.length === 0) {
                    this.guild.reportError(message, message.member.user, "There were no users mentioned in the command", namespace);
                    return;
                }

                var user: Discord.User;
                if (command !== "#namespace") {
                    for (var i in users) {
                        user = users[i];
                        if (this.guild.guildPermissions[namespace][command].permissionUserOverrides.includes(user.id)) {
                            let arrayPos = this.guild.guildPermissions[namespace][command].permissionUserOverrides.indexOf(user.id);
                            this.guild.guildPermissions[namespace][command].permissionUserOverrides.splice(arrayPos, 1);
                        } else {
                            console.log("User does not already have permissions"); //Maybe let cmd source know?
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for the command: " + command + " on the namespace: " + namespace + " have been removed for the specified users");
                } else {
                    let commands = this.guild.guildPermissions[namespace];
                    for (var aCommand in commands) {
                        for (var i in users) {
                            user = users[i];
                            if (this.guild.guildPermissions[namespace][aCommand].permissionUserOverrides.includes(user.id)) {
                                let arrayPos = this.guild.guildPermissions[namespace][aCommand].permissionUserOverrides.indexOf(user.id);
                                this.guild.guildPermissions[namespace][aCommand].permissionUserOverrides.splice(arrayPos, 1);
                            } else {
                                console.log("User does not already have permissions"); //Maybe let cmd source know?
                            }
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for all commands on the namespace: " + namespace + " have been removed for the specified users");
                }

                message.channel.send(response);
                break;
            case "removeRoleOverride":
                roles = message.mentions.roles.array();
                if (roles.length === 0) {
                    this.guild.reportError(message, message.member.user, "There were no roles mentioned in the command", namespace);
                    return;
                }

                var role: Discord.Role;
                if (command !== "#namespace") {
                    for (var i in roles) {
                        role = roles[i];
                        if (this.guild.guildPermissions[namespace][command].permissionRoleOverrides.includes(role.id)) {
                            let arrayPos = this.guild.guildPermissions[namespace][command].permissionRoleOverrides.indexOf(role.id);
                            this.guild.guildPermissions[namespace][command].permissionRoleOverrides.splice(arrayPos, 1);
                        } else {
                            console.log("User does not already have permissions"); //Maybe let cmd source know?
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for the command: " + command + " on the namespace: " + namespace + " have been removed for the specified roles");
                } else {
                    let commands = this.guild.guildPermissions[namespace];
                    for (var aCommand in commands) {
                        for (var i in roles) {
                            role = roles[i];
                            if (this.guild.guildPermissions[namespace][aCommand].permissionRoleOverrides.includes(role.id)) {
                                let arrayPos = this.guild.guildPermissions[namespace][aCommand].permissionRoleOverrides.indexOf(role.id);
                                this.guild.guildPermissions[namespace][aCommand].permissionRoleOverrides.splice(arrayPos, 1);
                            } else {
                                console.log("User does not already have permissions"); //Maybe let cmd source know?
                            }
                        }
                    }
                    this.database.updatePermissionObject(this.guild.guildPermissions);
                    response = createEmbed("Permissions for all commands on the namespace: " + namespace + " have been removed for the specified roles");
                }
                
                message.channel.send(response);
                break;
            default:
                this.guild.reportError(message, message.member.user, "There are no subcommand for permissions matching: " + subCommand, namespace);
                break;
        }
    }
    //Changes the bot command
    private botCommand(cmd: string[], message: Discord.Message) {
        let subCommand: string = cmd[0];
        let args: string[] = cmd.slice(1);

        if (subCommand === "set" && args[0] != undefined) this.guild.guildSettings["botCommand"] = args[0];
        this.database.updateGuildSettings(this.guild.guildSettings);

        let response = createEmbed("The bot root command has been set to: " + args[0]);
        message.channel.send(response);
    }
    //Handles loaded extensions
    private extensions(cmd: string[], message: Discord.Message) {
        let subCommand: string = cmd[0]
        let args: string[] = cmd.slice(1);
        let availableExtensions: string[] = mapDir("./extensions/");
        let extension: string = args[0] + ".js";
        let loadedExtensions: string[] = this.guild.guildSettings["extensions"];
        let response: Discord.RichEmbed;

        switch(subCommand) {
            case "enable":
                //Make it possible to enable multiple extensions with a single command
                if (!availableExtensions.includes(extension)) {
                    //Tell the user that there are no extensions that match
                    this.guild.reportError(message, message.member.user, "Error: there are no extensions matching: " + extension, namespace);
                    return;
                } 
                if (loadedExtensions.includes(extension)) {
                     //Tell the user that the extension is already loaded 
                     this.guild.reportError(message, message.member.user, "Error: The extension: " + extension + " is already loaded!", namespace);
                }

                this.guild.guildSettings["extensions"].push(extension);
                this.database.updateGuildSettings(this.guild.guildSettings);

                this.guild.reloadExtensions();

                response = createEmbed("The Extension: " + args[0] + " has been enabled!");
                message.channel.send(response);

                break;
            case "disable":
                //Make it possible to disable multiple extensions with a single command
                if (!availableExtensions.includes(extension)) {
                    //Tell the user that there are no extensions that match
                    this.guild.reportError(message, message.member.user, "Error: there are no extensions matching: " + extension, namespace);
                    return;
                }  
                if (!loadedExtensions.includes(extension)) {
                    //Tell the user that there are no extensions that match 
                    this.guild.reportError(message, message.member.user, "Error: The extension: " + extension + " is not currently active!", namespace);
                }

                console.log(loadedExtensions);
                let arrayPos = this.guild.guildSettings["extensions"].indexOf(extension);
                this.guild.guildSettings["extensions"].splice(arrayPos, 1);
                console.log(this.guild.guildSettings["extensions"]);
                this.database.updateGuildSettings(this.guild.guildSettings);

                response = createEmbed("The Extension: " + args[0] + " has been disabled!");
                message.channel.send(response);

                this.guild.reloadExtensions();
                break;
            case "listEnabled":
                let enabledExtensions: string = "";
                for (var i in loadedExtensions) {
                    let extension = loadedExtensions[i];
                    enabledExtensions = enabledExtensions + extension + "\n";
                }

                response = createEmbed(enabledExtensions, {"title": "These are the extensions currently enabled on this server:"});
                message.channel.send(response);
                break;
            case "listAvailable":
                let availableExtensionsList: string = "";
                for (var i in availableExtensions) {
                    let extension = availableExtensions[i];
                    availableExtensionsList = availableExtensionsList + extension + "\n";
                }

                response = createEmbed(availableExtensionsList, {"title": "These are the extensions currently available on this server:"});
                message.channel.send(response);
                break;
            case "reload":
                this.guild.reloadExtensions();
                response = createEmbed("All extensions on this server has been reloaded");
                message.channel.send(response);
                break;
            default:
                this.guild.reportError(message, message.member.user, "There is no subcommand for this extensions matching: " + subCommand, namespace);
                break;
        }
    }
    //Handles commands and aliases
    private commands(cmd: string[], message: Discord.Message) {
        let subCommand: string = cmd[0];
        let args: string[] = cmd.slice(1); 
        
        let namespace: string = args[0];
        let command: string = args[1];
        let arg: string = args[2];
        
        let response: Discord.RichEmbed;

        switch (subCommand) {
            //o!admin setAlias <namespace> <command> <alias>
            case "setAlias":
                let thisAlias = {namespace: namespace, command: command};
                this.guild.guildSettings.commandAliases[arg] = thisAlias;
                this.database.updateGuildSettings(this.guild.guildSettings);

                response = createEmbed("The alias: " + arg + " has been set for the command: " + command + " on the namespace: " + namespace);
                message.channel.send(response);
                break;
            //o!admin removeAlias <namespace> <command>
            case "removeAlias":
                arg = namespace;
                delete this.guild.guildSettings.commandAliases[arg];
                this.database.updateGuildSettings(this.guild.guildSettings);

                response = createEmbed("The alias: " + arg + " has been deleted");
                message.channel.send(response);
                break;
            //o!admin requireNamespace <namespace> true/false
            case "requireNamespace":
                if (command === "false") {
                    
                    let commands: string[] = []; 
                    for (var aCommand in this.guild.loadedExtensions[namespace].exposedCommands) {
                        commands.push(aCommand);
                    };

                    if (commands === undefined) return; //Userfeedback needed here

                    for (var i in commands) {
                        let thisAlias = {namespace: namespace, command: commands[i]};
                        this.guild.guildSettings.commandAliases[commands[i]] = thisAlias;
                    }

                    this.database.updateGuildSettings(this.guild.guildSettings);
                    let response = createEmbed("Command on namespace: " + namespace + " no longer require the namespace in the command");
                    message.channel.send(response);
                } else if (command === "true") {

                    let commands: string[] = []; 
                    for (var aCommand in this.guild.loadedExtensions[namespace].exposedCommands) {
                        commands.push(aCommand);
                    };

                    if (commands === undefined) return;

                    for (var i in commands) {
                        delete this.guild.guildSettings.commandAliases[commands[i]];
                    }

                    this.database.updateGuildSettings(this.guild.guildSettings);
                    let response = createEmbed("Command on namespace: " + namespace + "now require the namespace in the command");
                    message.channel.send(response);
                }
                break;
        }
    }
}

export {namespace}
export {events}
export {initExtension}
export {Extension}