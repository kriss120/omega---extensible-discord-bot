import Discord = require('discord.js');
import Winston = require('winston');
import {Guild} from './guild';
const auth = require('./auth.json');

const logger = Winston.createLogger({
    level: 'info',
    format: Winston.format.combine(Winston.format.timestamp(), Winston.format.prettyPrint()),
    //defaultMeta: {service: 'user-service'},
    transports: [
        //new Winston.transports.Console(),
        new Winston.transports.File({filename: 'error.log', level: 'error'}),
        new Winston.transports.File({filename: 'combined.log'})
    ]
});

const client = new Discord.Client();
var guildList: {[id: string] : Guild} = {};

function validateGuild(guildID: string) {
    if (guildList[guildID] === undefined || guildList[guildID] === null) return false;
    return true;
}

//Connects bot to discord bot account.
client.login(auth.token);

//Logs connection error, needs to handle connection reset if not automatically resolved.
client.on('error', () => {
    
});

//Init bot
client.once('ready', async () => {
    //Log in all guilds and initialize them
    var connectedGuilds = client.guilds.array();
    for (var i = 0; i < connectedGuilds.length; i++) {
        var thisGuild = connectedGuilds[i];
        console.log("Guild with ID: " + thisGuild.id + " has connected to Omega");
        var newGuild = new Guild(thisGuild);
        await newGuild.init();
        guildList[thisGuild.id] = newGuild;
    }
    console.log('Omega is Ready');
});

//Handles event when a discord server adds the bot
client.on('guildCreate', async (guild) => {
    if (guildList[guild.id] !== undefined) return;
    console.log("Guild with ID: " + guild.id + " has connected to Omega");
    let newGuild = new Guild(guild);
    await newGuild.init();
    guildList[guild.id] = newGuild;
});

//Handles event when a discord server kicks the bot
client.on('guildDelete', async (guild) => {
    if (guildList[guild.id] === undefined) return;
    delete guildList[guild.id];
    console.log("Guild with ID: " + guild.id + " has disconnected from Omega");
});

//EVENTS EXPOSED TO GUILDS AND EXTENSIONS:
//MESSAGE_CREATE
client.on('message', async (message: Discord.Message) => {
    //Forward message event
    if (message.member.user.bot) return;

    let guildID = message.guild.id;
    if (guildID === undefined) return;
    if (!validateGuild(guildID)) return;

    var packet: {[packetType: string]: any} = {};
    packet["message"] = message;

    guildList[guildID].resolveEvent(packet, "MESSAGE_CREATE");
});
//messageUpdate
client.on('messageUpdate', async (message: Discord.Message) => {
    //Forward message delete event
});
//MESSAGE_REACTION_ADD
client.on('messageReactionAdd', async (messageReaction: Discord.MessageReaction, user: Discord.User) => {
    if (user.bot) return;
    
    let guildID = messageReaction.message.guild.id;
    if (guildID === undefined) return;
    if (!validateGuild(guildID)) return;

    var packet: {[packetType: string]: any} = {};
    packet["messageReaction"] = messageReaction;
    packet["user"] = user;

    guildList[guildID].resolveEvent(packet, "MESSAGE_REACTION_ADD");
});
//MESSAGE_REACTION_REMOVE
client.on('messageReactionRemove', async (messageReaction: Discord.MessageReaction, user: Discord.User) => {
    if (user.bot) return;
    
    let guildID = messageReaction.message.guild.id;
    if (guildID === undefined) return;
    if (!validateGuild(guildID)) return;

    var packet: {[packetType: string]: any} = {};
    packet["messageReaction"] = messageReaction;
    packet["user"] = user;

    guildList[guildID].resolveEvent(packet, "MESSAGE_REACTION_REMOVE");
});
//GUILD_MEMBER_ADD
client.on('guildMemberAdd', async (member) => {
    if (member.user.bot) return;

    let guildID = member.guild.id;
    if (guildID === undefined) return;
    if (!validateGuild(guildID)) return;

    var packet: {[packetType: string]: any} = {};
    packet["member"] = member;
    
    guildList[guildID].resolveEvent(packet, "GUILD_MEMBER_ADD");
});
//GUILD_MEMBER_REMOVE
client.on('guildMemberRemove', async (member) => {
    if (member.user.bot) return;

    let guildID = member.guild.id;
    if (guildID === undefined) return;
    if (!validateGuild(guildID)) return;

    var packet: {[packetType: string]: any} = {};
    packet["member"] = member;
    
    guildList[guildID].resolveEvent(packet, "GUILD_MEMBER_REMOVE");
}); 