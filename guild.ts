import {mapDir,getStringArguments, createEmbed} from './omegaToolkit';
import {Extension} from './extension';
import {OmegaDB} from './omegaDB';
import Discord = require('discord.js');

class Guild {
    guild: Discord.Guild;
    id: string;
    database: OmegaDB;
    loadedExtensions: {[namespace: string]: Extension};
    extensionEventList: {[event: string]: string[]};
    guildSettings: {[settingID: string]: any};
    guildPermissions: {[namespace: string]: {[command: string]: {permissionLevel: number, permissionRoleOverrides: string[], permissionUserOverrides: string[]}}};

    constructor(aGuild: Discord.Guild) {
        this.guild = aGuild;
        this.id = this.guild.id;
        this.loadedExtensions = {};
        this.extensionEventList = {};
        this.database = new OmegaDB(this.id);
        this.guildSettings = {};
        this.guildPermissions = {};
    }

    async init() {
        //Init Guild mongoDB Collection
        await this.database.init();

        //First time init
        let settingsStatus = await this.database.checkSettings();
        let permissionsStatus = await this.database.checkPermissions();
        if (!settingsStatus) await this.database.generateGuildSettings({'botCommand': 'ob!', 'extensions': [], 'commandAliases': {}});
        if (!permissionsStatus) await this.database.generatePermissionObject({})

        //Init guild settings
        this.guildSettings = await this.database.getGuildSettings();
        this.guildPermissions = await this.database.getPermissionObject();

        //Update old databases, remove in next release
        if (this.guildSettings["commandAliases"] === undefined) {
            this.guildSettings["commandAliases"] = {};
            this.database.updateGuildSettings(this.guildSettings);
        }

        //Load AdminTool
        this.loadExtension("./omegaAdmin.js");

        //Load Extensions
        let availableExtensions: string[] = mapDir('./extensions/');
        var extension: string;
        for (var i in availableExtensions) {
            extension = availableExtensions[i];
            if (this.guildSettings.extensions.includes(extension)) {
                console.log("Loading extension: " + extension + " on guild with ID: " + this.id);
                this.loadExtension("./extensions/" + extension);
            }
        }
        
        return;
    }

    //Method for reloading and reiniting all extension in a guild
    async reloadExtensions() {
        console.log("Reloading extensions on guild with ID: " + this.id);
        let availableExtensions: string[] = mapDir('./extensions/');
        var extension: string;

        for (var extension in this.loadedExtensions) {
            delete this.loadedExtensions[extension];
        }

        for (var i in availableExtensions) {
            extension = availableExtensions[i];
            if (this.guildSettings.extensions.includes(extension)) {
                console.log("Loading extension: " + extension + " on guild with ID: " + this.id);
                this.loadExtension("./extensions/" + extension);
            }
        }
    }

    async loadExtension(extensionPath: string) {
        //Load and init extension
        //This needs to read in from an extension folder
        const newExtension = require(extensionPath);
        var thisExtension: Extension  = newExtension.initExtension(this, this.database);
        await thisExtension.init();

        //Add extension to loadedExtensions
        this.loadedExtensions[newExtension.namespace] = thisExtension;

        //Add Extension to extensionEventList
        let extensionEvents: string[] = newExtension.events;
        var event: string;
        for (var i in extensionEvents) {
            event = extensionEvents[i]; 
            if (this.extensionEventList[event] === undefined || this.extensionEventList[event] === null) this.extensionEventList[event] = [];
            this.extensionEventList[event].push(newExtension.namespace);
        }

        //Generate empty permissions if they are missing and there are exposed commands
        let extensionCommands: string[] = [];
        for (var command in thisExtension.exposedCommands) {
            extensionCommands.push(command);
        }

        var command: string;
        for (var i in extensionCommands) {
            command = extensionCommands[i];
            if (this.guildPermissions[newExtension.namespace] === undefined) {
                this.guildPermissions[newExtension.namespace] = {};
                this.guildPermissions[newExtension.namespace][command] = {permissionLevel: 8, permissionRoleOverrides: [], permissionUserOverrides: []};

                this.database.updatePermissionObject(this.guildPermissions);
                //console.log("Updated Permission");
            } else if (this.guildPermissions[newExtension.namespace][command] === undefined) {
                this.guildPermissions[newExtension.namespace][command] = {permissionLevel: 8, permissionRoleOverrides: [], permissionUserOverrides: []};
                this.database.updatePermissionObject(this.guildPermissions);
                //console.log("Updated Permissions");
            }
        }
    }

    //Resolve event type and send to correct extension(s) 
    //Messages should be checked for commands, and if they contain commands they should execute the corresponding extension functionality.
    //Available commands and functionality is to be loaded into the guild on init, stored universally. All extensions need a "namespace" to avoid crosstalk
    //between commands. Command structure should be something like "o!poll create"
    //"o!" targets omega itself to make the bot expect a command, "poll" is the namespace of the extension targeted and "create" targets the specific function.
    async resolveEvent(packet: any, type: string) {
        let eventType: string = type;
        let extensionList: string[];
        var extension: string;

        if (eventType === "MESSAGE_CREATE") {
            let message: Discord.Message = packet["message"];
            this.resolveCommand(getStringArguments(message.content, this.guildSettings.botCommand), message);
        }
        
        if (this.extensionEventList[eventType] === undefined || this.extensionEventList[eventType] === null) return;
        
        extensionList = this.extensionEventList[eventType];
        for (var i in extensionList) {
            extension = extensionList[i];
            this.loadedExtensions[extension].resolveEvent(packet, eventType);
        }
    }

    //Resolves command and executes if valid
    resolveCommand(args: string[], message: Discord.Message) {
        //Check for compatible command namespace
        //Forward command args to extension with corresponding namespace
        //console.log("Resolving command");
        if (args.length === 0) return;

        if (args[0] === "help") this.generateHelp(message);

        //console.log("Checking for valid command namespace");
        if ((this.loadedExtensions[args[0]] !== undefined) && (this.loadedExtensions[args[0]] !== null)) {
            //console.log("Forwarding command");
            if (!this.resolvePermissions(args[0], args[1], message.member)) return;
            this.loadedExtensions[args[0]].eventOnCommandMessage(args.slice(1), message);
        } else if (this.guildSettings.commandAliases[args[0]] !== undefined) {
            let namespace: string = this.guildSettings.commandAliases[args[0]]["namespace"];
            let command: string = this.guildSettings.commandAliases[args[0]]["command"];

            if ((this.loadedExtensions[namespace] !== undefined) && (this.loadedExtensions[namespace] !== null)) {
                //console.log("Forwarding command");
                if (!this.resolvePermissions(namespace, command, message.member)) return;
                args[0] = command; //Replaces the alias with the correct command
                this.loadedExtensions[namespace].eventOnCommandMessage(args, message);
            }
        }
    }
    
    //Resolve permissions
    resolvePermissions(extensionName: string, command: string, member: Discord.GuildMember) {
        //console.log("Resolving permissions");
        let permissionObject = this.guildPermissions[extensionName];
        if (permissionObject === undefined) return true;

        let cmdPermissionObject = permissionObject[command];
        if (cmdPermissionObject === undefined) return true;
       
        
        if (member.permissions.has(cmdPermissionObject.permissionLevel, true)) return true;
        
        let cmdPermissionRoleOverrides: string[] = cmdPermissionObject.permissionRoleOverrides;
        for (var i in cmdPermissionRoleOverrides) {
            if (member.roles.has(cmdPermissionRoleOverrides[i])) return true;
        }

        let cmdPermissionUserOverrides: string[] = cmdPermissionObject.permissionUserOverrides;
        if (cmdPermissionUserOverrides.includes(member.id)) return true;
        
        //console.log("User does not have permission");
        return false;
    }

    //Useful textbased help interface with information about commands and usage
    generateHelp(message: Discord.Message) {
        var commands: {[namespace: string]: {[command: string]: {commandUsage: string, commandInfo: string}}} = {};
        for (var extension in this.loadedExtensions) {
            commands[extension] = this.loadedExtensions[extension].exposedCommands as {[command: string]: {commandUsage: string, commandInfo: string}};
        }

        var fields: {name: string, value: string, inline: boolean}[] = [];

        for (var namespace in commands) {
            if (namespace !== "admin") {
                var field: {name: string, value: string, inline: boolean} = {name: namespace, value: "", inline: false};
                var value: string = "";
                
                let someCommands = commands[namespace];
                for (var aCommand in someCommands) {
                    let thisCommand = someCommands[aCommand];
                    value = value + "**" + aCommand + ":**\n" + thisCommand.commandUsage + "\n" + thisCommand.commandInfo + "\n"; 
                }

                field.value = value;
                fields.push(field);
            }
        }

        let embedOptions = {
            title: "**Commands and usage:**",
            fields: fields,
        }
        let response = createEmbed("These are the available commands and namespaces,\nall commands are prefaced with " + this.guildSettings.botCommand, embedOptions);

        message.channel.send(response);
    }

    //Simple method for simple error reporting for extensions
    //Should report errors to winston or a logger in the future on a per guild basis
    reportError(discordMessage: Discord.Message, user: Discord.User, error: string, namespace?: string) {
        let embedOptions: {[option: string]: any} = {};

        if (namespace) embedOptions["title"] = namespace;
        embedOptions["color"] = "0xFF0000"

        let message: Discord.RichEmbed = createEmbed(error, embedOptions);
        discordMessage.channel.send(message);
    }   
}

export {Guild}