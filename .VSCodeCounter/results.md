# Summary

Date : 2020-07-21 02:22:41

Directory /home/kristoffer/Documents/Code Projects/Devember Omega Rewrite/OmegaProjectRewrite

Total : 16 files,  2696 codes, 185 comments, 399 blanks, all 3280 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript | 11 | 1,926 | 185 | 397 | 2,508 |
| JSON | 4 | 762 | 0 | 2 | 764 |
| JSON with Comments | 1 | 8 | 0 | 0 | 8 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 16 | 2,696 | 185 | 399 | 3,280 |
| extensions | 5 | 1,105 | 56 | 215 | 1,376 |
| extensions/ffxiv_integration_extension | 1 | 4 | 0 | 0 | 4 |

[details](details.md)