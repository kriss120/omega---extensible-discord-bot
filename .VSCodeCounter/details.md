# Details

Date : 2020-07-21 02:22:41

Directory /home/kristoffer/Documents/Code Projects/Devember Omega Rewrite/OmegaProjectRewrite

Total : 16 files,  2696 codes, 185 comments, 399 blanks, all 3280 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [Omega.ts](/Omega.ts) | TypeScript | 93 | 34 | 48 | 175 |
| [auth.json](/auth.json) | JSON | 3 | 0 | 0 | 3 |
| [eventResolver.ts](/eventResolver.ts) | TypeScript | 29 | 9 | 5 | 43 |
| [extension.ts](/extension.ts) | TypeScript | 51 | 18 | 16 | 85 |
| [extensions/audioplayback_extension.ts](/extensions/audioplayback_extension.ts) | TypeScript | 78 | 5 | 22 | 105 |
| [extensions/example_extension.ts](/extensions/example_extension.ts) | TypeScript | 62 | 1 | 17 | 80 |
| [extensions/ffxiv_integration_extension.ts](/extensions/ffxiv_integration_extension.ts) | TypeScript | 666 | 43 | 116 | 825 |
| [extensions/ffxiv_integration_extension/ffxiv_auth.json](/extensions/ffxiv_integration_extension/ffxiv_auth.json) | JSON | 4 | 0 | 0 | 4 |
| [extensions/poll_extension.ts](/extensions/poll_extension.ts) | TypeScript | 295 | 7 | 60 | 362 |
| [guild.ts](/guild.ts) | TypeScript | 127 | 31 | 33 | 191 |
| [omegaAdmin.ts](/omegaAdmin.ts) | TypeScript | 287 | 15 | 37 | 339 |
| [omegaDB.ts](/omegaDB.ts) | TypeScript | 132 | 18 | 26 | 176 |
| [omegaRewrite.code-workspace](/omegaRewrite.code-workspace) | JSON with Comments | 8 | 0 | 0 | 8 |
| [omegaToolkit.ts](/omegaToolkit.ts) | TypeScript | 106 | 4 | 17 | 127 |
| [package-lock.json](/package-lock.json) | JSON | 736 | 0 | 1 | 737 |
| [package.json](/package.json) | JSON | 19 | 0 | 1 | 20 |

[summary](results.md)