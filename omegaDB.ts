import mongo = require('mongodb');
//Module simplifying database interaction for extensions

class OmegaDB {
    url: string;
    guildId: string;
    database: mongo.Db;

    constructor(id: string) {
        this.url = "mongodb://localhost:27017";
        this.guildId = id;
        this.database = {} as mongo.Db;
    }

    async init() {
        //Connect DB
        let mongoClient = mongo.MongoClient;
        var db = await mongoClient.connect(this.url, {useUnifiedTopology: true});
        this.database = db.db("omegaDB");

        //Check if guild collection already exists
        var collectionExists = false;
        let collections = await this.database.listCollections({}, {"nameOnly": true}).toArray();

        for (var collection in collections) {
            if (collections[collection].name == this.guildId) {
                collectionExists = true;
                break;
            }
        }

        if (collectionExists) return;

        //If guild collection is missing, create new collection
        await this.database.createCollection(this.guildId);
    }

    //Checks for settings and permissions
    async checkSettings() {
        let hasSettings: boolean = await this.omegaDBDocumentExists({docID: "settings"});
        if (hasSettings) return true;

        return false;
    }
    async checkPermissions() {
        let hasPermissions: boolean = await this.omegaDBDocumentExists({docID: "permissions"});
        if (hasPermissions) return true;

        return false;
    }
    async checkExtension(extensionNamespace: string) {
        let hasExtension: boolean = await this.omegaDBDocumentExists({docID: extensionNamespace});
        if (hasExtension) return true;

        return false;
    }

    //Methods for generating documents on first time use
    async generateGuildSettings(guildSettings: any) {
        let guildSettingsObject = {docID: "settings", document: guildSettings};
        await this.omegaDBSendDocument(guildSettingsObject);
    }
    async generatePermissionObject(guildPermissions: any) {
        let guildPermissionsObject = {docID: "permissions", document: guildPermissions};
        await this.omegaDBSendDocument(guildPermissionsObject);
    }
    async generateExtensionSettings(extension_namespace: string, extensionSettings: any) {
        let extensionSettingsObject = {docID: extension_namespace, document: extensionSettings};
        await this.omegaDBSendDocument(extensionSettingsObject);
    }

    //Implement system for deleting collection if guild kicks omega from server

    //Methods relating to guilds
    async getGuildSettings() {
        var guildSettings = await this.omegaDBQueryOne({docID: "settings"});
        return guildSettings["document"];
    }
    async updateGuildSettings(guildSettings: any) {
        let updatedGuildSettings = {$set: {document: guildSettings}};
        await this.omegaDBUpdateDocument({docID: "settings"}, updatedGuildSettings);
    }
    async deleteGuildSettings() {
        //Implement functionality for deleting guild settings
    }

    //Methods for getting permission object
    async getPermissionObject() {
        var guildPermissions = await this.omegaDBQueryOne({docID: "permissions"});
        return guildPermissions["document"];
    }
    async updatePermissionObject(permissionObject: any) {
        let updatedPermissions = {$set: {document: permissionObject}};
        await this.omegaDBUpdateDocument({docID: "permissions"}, updatedPermissions);
    }
    async deletePermissionObject() {
        //Implement functionality for deleting permissionObject
    }

    //Methods relating to extensions
    async getExtensionSettings(extension_namespace: string) {
        var extensionSettings = await this.omegaDBQueryOne({docID: extension_namespace});
        return extensionSettings["document"];
    }
    async updateExtensionSettings(extension_namespace: string, extensionSettings: any) {
        let updatedExtensionSettings = {$set: {document: extensionSettings}};
        await this.omegaDBUpdateDocument({docID: extension_namespace}, updatedExtensionSettings);
    }
    async deleteExtensionSettings(extension_namespace: string) {
        //Implement functionality for deleting extension settings
    }

    //Query for specific document in collection
    async omegaDBQUery(query: any) {
        var queryResult = await this.database.collection(this.guildId).find(query).toArray;
        return queryResult;
    }
    async omegaDBQueryOne(query: any) {
        var queryResult = await this.database.collection(this.guildId).findOne(query);
        return queryResult;
    }

    //Upadte specified document
    async omegaDBUpdateDocument(query: any, updateObj: any) {
        try {
            this.database.collection(this.guildId).updateOne(query, updateObj, (err, res) => {
                if (err) {
                    throw (err);
                }
            });
        } catch (err) {
            console.log(err);
            return false;
        }

        return true;
    }

    //Send specified document to database
    async omegaDBSendDocument(obj: any) {
        try {
            await this.database.collection(this.guildId).insertOne(obj);
        } catch (err) {
            console.log(err);
            return false;
        }

        return true;
    }

    //Delete specified document
    async omegaDBDeleteDocument(query: any) {
        try {
            this.database.collection(this.guildId).deleteOne(query, (err, obj) => {
                if (err) {
                    throw (err);
                }
            });
        } catch (err) {
            console.log(err);
            return false;
        }

        return true;
    }

    //Check if document exists
    async omegaDBDocumentExists(query: any) {
        var document = await this.database.collection(this.guildId).findOne(query);
        if (document === null) return false;
        
        return true;
    }
}

export {OmegaDB}