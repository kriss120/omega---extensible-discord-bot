//Needs to export information about:
//1. Namespace
//2. A list of events that should be forwarded to the extension
//3. Initializer for extension
import {OmegaDB} from "./omegaDB";
import Discord = require('discord.js');
import {Guild} from "./guild";

const namespace: string = "extension"; //Declares the extensions namespace
const events: string[] = ["MESSAGE_CREATE", "MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE"]; //Declares what events the extension wants

//Commands that should be exposed for auto generating initial permissions
const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {
    exampleCommand1: {
        commandUsage: "namespace command <args>", 
        commandInfo: "This is an example of exposed commands"
    },
    exampleCommand2: {
        commandUsage: "namespace command <args",
        commandInfo: "This is an example of exposed commands"
    }  
}; 

//Initializes a new extension class and returns it to the caller
function initExtension(guild: Guild, database: OmegaDB) {
    return new Extension(guild, database, commands);
}

//Basic Extension Structure
class Extension {
    guild: Guild;
    database: OmegaDB;
    exposedCommands?: {[command: string]: {commandUsage: string, commandInfo: string}};

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        if (commands) this.exposedCommands = commands;
        this.guild = guild;
        this.database = database;
    }

    async init() {
        //Used to init when there are database queries to wait for
    }

    //List commands
    listCommands() {

    }

    //Handles and resolves commands
    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        //Check if command is valid
        //Check if user has permissions
        return;
    }

    async resolveEvent(packet: any, type: string) {
        //Resolve events
        return;
    }

    //Handles the MESSAGE_CREATE event
    eventOnMessage(packet: any) {
        return;         
    }

    //Handles the MESSAGE_REACTION_ADD event
    eventOnMessageReactionAdd(packet: any) {
        return;
    }
    
    //Handles the MESSAGE_REACTION_REMOVE event
    eventOnMessageReactionRemove(packet: any) {
        return;
    }

    //Handles the GUILD_MEMBER_ADD event
    eventOnGuildMemberAdd(member: Discord.GuildMember) {
        return;
    }

    //Handles the GUILD_MEMBER_REMOVE event
    eventOnGuildMemberRemove(member: Discord.GuildMember) {
        return;
    }

    //For use when extension wants to utilize events not supported by Omega by default
    eventOnRaw(packet: any) {
        return;
    }
}

export {namespace}
export {events}
export {initExtension}
export {Extension}