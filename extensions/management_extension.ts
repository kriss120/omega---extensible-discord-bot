/*
    Extension providing advanced, powerfull and usefull moderation tools to discord guilds
    Planned features:
    - Powerfull logging tool: Should log important events in an easy to read log. Should be customizable.
    - Auto Guild Management: Tool for automating guild management, e.g auto roles, etc.
    - Configurable moderation layer: More on this later.
    - Support ticket system: More on this to come
*/

import {Extension} from '../extension';
import {OmegaDB} from "../omegaDB";
import Discord = require('discord.js');
import {Guild} from "../guild";

const namespace: string = "mod";
const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {};
const events: string[] = ["MESSAGE_CREATE", 
                          "MESSAGE_REACTION_ADD", 
                          "MESSAGE_REACTION_REMOVE", 
                          "GUILD_MEMBER_ADD", 
                          "GUILD_MEMBER_REMOVE"];

function initExtension(guild: Guild, database: OmegaDB) {
    return new GuildManager(guild, database, commands);
}

class GuildManager extends Extension {
    extensionData: {[dataType: string]: any};
    enabledTools: string[];
    toolList: {[tool: string]: any};
    guildLogger?: GuildLogger;

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        super(guild, database, commands);
        this.extensionData = {};
        this.enabledTools = [];
        this.toolList = {};
    }

    async init() {
        //Init database integration and enabled management tools.
        let extensionDBStatus = await this.database.checkExtension(namespace);
        if (!extensionDBStatus) await this.database.generateExtensionSettings(namespace, {enabledTools: []});

        this.extensionData = await this.database.getExtensionSettings(namespace);
        this.enabledTools = this.extensionData["enabledTools"];

        if (this.enabledTools.includes("GuildLogger")) {
            this.toolList["GuildLogger"] = new GuildLogger();
        }
    }

    listCommands() {
        return ["Example Extension"];
    }

    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        console.log(cmd);
    }

    async resolveEvent(packet: {[dataType: string]: any}, type: string) {
        let eventType = type;

        switch (eventType) {
            case "MESSAGE_CREATE":
                this.eventOnMessage(packet["message"]);
                break;
            case "MESSAGE_REACTION_ADD":
                this.eventOnMessageReactionAdd(packet);
                break;
            case "MESSAGE_REACTION_REMOVE":
                this.eventOnMessageReactionRemove(packet);
                break;
            case "GUILD_MEMBER_ADD":
                this.eventOnGuildMemberAdd(packet["member"]);
                break;
            case "GUILD_MEMBER_REMOVE":
                this.eventOnGuildMemberRemove(packet["member"]);
                break;
            default:
                console.log(eventType);
                break;
        }

        return;
    }

    eventOnMessage(message: Discord.Message) {
        //Check if logger is active and forward event if true
        if (this.toolList["GuildLogger"] !== undefined) {
            let packet: {[dataType: string]: any} = {"message": message};
            this.toolList["GuildLogger"].resolveLoggable(packet, "MESSAGE_CREATE");
        }
    }

    eventOnMessageReactionAdd(packet: {[dataType: string]: any}) {
        //Logic handling reaction add events
        //Check if logger is active and forward event if true
        if (this.toolList["GuildLogger"] !== undefined) {
            this.toolList["GuildLogger"].resolveLoggable(packet, "MESSAGE_REACTION_ADD");
        }
    }

    eventOnMessageReactionRemove(packet: {[dataType: string]: any}) {
        //Logic handling reaction remove events
        if (this.toolList["GuildLogger"] !== undefined) {
            this.toolList["GuildLogger"].resolveLoggable(packet, "MESSAGE_REACTION_REMOVE");
        }
    }

    eventOnGuildMemberAdd(member: Discord.GuildMember) {
        //Logic handling member add events
        //Check if logger is active and forward event if true
        if (this.toolList["GuildLogger"] !== undefined) {
            let packet: {[dataType: string]: any} = {"member": member};
            this.toolList["GuildLogger"].resolveLoggable(packet, "GUILD_MEMBER_ADD");
        }
    }

    eventOnGuildMemberRemove(member: Discord.GuildMember) {
        //Logic handling member remove events
        //Check if logger is active and forward event if true
        if (this.toolList["GuildLogger"] !== undefined) {
            let packet: {[dataType: string]: any} = {"member": member};
            this.toolList["GuildLogger"].resolveLoggable(packet, "GUILD_MEMBER_REMOVE");
        }
    }

    eventOnRaw() {
        return;
    }
}

//Powerfull and configurable logging tool
/*
    Loggable events:
    - User join/leave guild
*/
class GuildLogger {
    loggerConfig: {[setting: string]: any};
    constructor() {
        this.loggerConfig = {};
    }

    async init() {

    }

    resolveLoggable(packet: {[dataType: string]: any}, loggableType: string) {
        return;
    }


}

//Auto Moderation tool for automating responses to events like new members etc.
class AutoModerator {

}

//Moreation tool
class ModerationTool {

}

//Support tool for ticketing etc.
class SupportTool {

}

export {namespace}
export {events}
export {initExtension}