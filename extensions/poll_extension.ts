import {Extension} from '../extension';
import {OmegaDB} from "../omegaDB";
import Discord = require('discord.js');
import {Guild} from "../guild";
import {createEmbed, checkFlags} from '../omegaToolkit';

const namespace: string = "poll";
const events: string[] = ["MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE"];

const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {
    create: {
        commandUsage: "`" + namespace + " create <pollId> <pollName> <option1, ... option9>`", 
        commandInfo: "Creates a poll with the supplied poll id, poll name, and options"
    },
    post: {
        commandUsage: "`" + namespace + " post <pollId>`",
        commandInfo: "Post the poll with the corresponding poll id"
    },
    end: {
        commandUsage: "`" + namespace + " end <pollId>`",
        commandInfo: "Ends the poll with the corresponding poll id, returning a message with the results"
    },
    list: {
        commandUsage: "`" + namespace + " list`",
        commandInfo: "Lists all available polls"
    }
}; 

function initExtension(guild: Guild, database: OmegaDB) {
    return new Polly(guild, database, commands);
}

class Polly extends Extension {
    settings: any;
    polls: {[pollId: string]: Poll};
    extensionData: {[dataType: string]: any};

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        super(guild, database, commands);
        this.settings = {};
        this.extensionData = {};
        this.polls = {} as {[pollId: string]: Poll};
    }

    async init() {
        let extensionDBStatus = await this.database.checkExtension(namespace);
        if (!extensionDBStatus) await this.database.generateExtensionSettings(namespace, {settings: {} ,polls: {}});

        this.extensionData = await this.database.getExtensionSettings(namespace);
        this.settings = this.extensionData["settings"];
        let polls: {[pollID: string]: any} = this.extensionData["polls"];

        if (polls.length === 0) return;
        for (var poll in polls) {
            var thisPoll = polls[poll];
            var newPoll = new Poll(thisPoll["pollID"], thisPoll["pollName"]);
            newPoll.pollData = thisPoll;

            if (newPoll.pollData.isPosted) {
                var fetchState: boolean = true;
                try {
                    newPoll.pollMessage = await this.getMessage(thisPoll["messageId"], thisPoll["channelId"]);
                } catch (error) {
                    if (error) fetchState = false;

                    let pollId: string = newPoll.pollData.pollId;

                    delete this.polls[pollId];
                    delete this.extensionData["polls"][pollId];
                    await this.database.updateExtensionSettings(namespace, this.extensionData);
                }
                
                if (newPoll.pollMessage !== undefined) {
                    await this.initPostedPoll(newPoll, newPoll.pollMessage);
                    this.polls[newPoll.pollData.pollId] = newPoll;
                }
            } else {
                this.polls[newPoll.pollData.pollId] = newPoll;
            }
        }
    }
    async getMessage(messageId: string, channelId: string) {
        let channels = this.guild.guild.channels.array();
        let channel: Discord.TextChannel = channels.find(element => element.id === channelId) as Discord.TextChannel;

        let message: Discord.Message = await channel.fetchMessage(messageId);
        return message;
    }
    async initPostedPoll(poll: Poll, message: Discord.Message) {
        if (poll.pollData.multivote) return;

        let reactions = message.reactions.array();
        for (var i in reactions) {
            let reaction = reactions[i];
            let userCollection = await reaction.fetchUsers();
            let users = userCollection.array();

            if (poll.pollData.pollOptions[reaction.emoji.name] !== undefined) {
                for (var i in users) {
                    let user = users[i];
                    if (poll.pollData.participants[user.id] !== undefined) {
                        let emoji: string = poll.pollData.participants[user.id];
                        if (reaction.emoji.name !== emoji && user.bot !== true) await reaction.remove(user.id);
                    } else {
                        poll.pollData.participants[user.id] = reaction.emoji.name;
                    }
                }
            } else {
                for (var i in users) {
                    let user = users[i];
                    await reaction.remove(user.id);
                }
            }
        }
    }

    listCommands() {

    }

    //Handles commands
    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        let checkedCmds: {flags: string[], args: string[]} = checkFlags(cmd);
        let flags: string[] = checkedCmds.flags;
        let command: string = checkedCmds.args[0];
        let args = checkedCmds.args.slice(1);

        switch (command) {
            case "create":
                this.createPoll(message, args, flags);
                break;
            case "post":
                this.postPoll(message, args, flags);
                break;
            case "end":
                this.endPoll(message, args, flags);
                break;
            case "list":
                this.listPolls(message, args, flags);
                break;
        }
    }
    createPoll(message: Discord.Message, args: string[], flags: string[]) {
        let pollId: string = args[0];
        let pollName: string = args[1];
        let pollOptions: string[] = args.slice(2);
        let multivote: boolean = false;

        if (pollOptions.length > 9) return; //Let user know there are too many options

        for (var i in flags) {
            switch (flags[i]) {
                case "multivote":
                    multivote = true;
                    break;
                default:
                    break;
            }
        }

        var newPoll = new Poll(pollId, pollName, multivote, pollOptions);
        this.polls[pollId] = newPoll;

        this.extensionData["polls"][pollId] = newPoll.pollData;
        this.database.updateExtensionSettings(namespace, this.extensionData);

        let response = createEmbed("The poll was successfully created!");
        message.channel.send(response);
    }
    async postPoll(message: Discord.Message, args: string[], flags: string[]) {
        let pollId: string = args[0];
        
        if (this.polls[pollId] === undefined) return; //Let user know there are no polls with that id

        let posted: boolean = await this.polls[pollId].postPoll(message.channel as Discord.TextChannel);
        if (posted) {
            this.extensionData["polls"][pollId] = this.polls[pollId].pollData;
            this.database.updateExtensionSettings(namespace, this.extensionData);
        }
        message.delete();
    }
    async endPoll(message: Discord.Message, args: string[], flags: string[]) {
        let pollId: string = args[0];

        if (this.polls[pollId] === undefined) return; //Let user know there are no polls with that id
        if (!this.polls[pollId].pollData.isPosted) return; //Let user know the poll has yet to be posted

        let channels = this.guild.guild.channels.array();
        let channel: Discord.TextChannel = channels.find(channel => channel.id === this.polls[pollId].pollData.channelId) as Discord.TextChannel;
        let pollMessage = await channel.fetchMessage(this.polls[pollId].pollData.messageId); 

        let reactions = pollMessage.reactions.array(); 
        let poll = this.polls[pollId];
        var results: string = "";
        for (var i in reactions) {
            let reaction = reactions[i];
            let emoji = reaction.emoji.name;
            results = results + emoji + " " + poll.pollData.pollOptions[emoji] + " got " + (reaction.count - 1) + " vote(s)\n"; 
        }

        let responseEmbed = createEmbed(results, {"title": "Results for: " + poll.pollData.pollName});
        message.channel.send(responseEmbed);
        
        pollMessage.delete();
        message.delete();

        delete this.extensionData["polls"][pollId];
        delete this.polls[pollId];
        this.database.updateExtensionSettings(namespace, this.extensionData);
    }
    listPolls(message: Discord.Message, args: string[], flags: string[]) {
        let response: string = "";
        for (var pollId in this.polls) {
            response = response + "**" + pollId + "**: " + this.polls[pollId].pollData.pollName + "\n";
        }

        let responseEmbed = createEmbed(response, {"title": "These are your stored polls"});
        message.channel.send(responseEmbed);
    }

    //Handles Events
    async resolveEvent(packet: any, type: string) {
        switch (type) {
            case "MESSAGE_REACTION_ADD":
                this.eventOnMessageReactionAdd(packet);
                break;
            case "MESSAGE_REACTION_REMOVE":
                this.eventOnMessageReactionRemove(packet);
                break;
        }
    }
    async eventOnMessageReactionAdd(packet: any) {
        let reaction: Discord.MessageReaction = packet["messageReaction"];
        let user: Discord.User = packet["user"];

        for (var pollId in this.polls) {
            let poll = this.polls[pollId];
            if (poll.pollData.messageId === reaction.message.id) {
                await this.pollReactionAdd(reaction, user, poll);
                return;
            }
        }
    }
    async eventOnMessageReactionRemove(packet: any) {
        let reaction: Discord.MessageReaction = packet["messageReaction"];
        let user: Discord.User = packet["user"];

        for (var pollId in this.polls) {
            let poll = this.polls[pollId];
            if (poll.pollData.messageId === reaction.message.id) {
                await this.pollReactionRemove(reaction, user, poll);
                return;
            }
        }
    }

    async pollReactionAdd(reaction: Discord.MessageReaction, user: Discord.User, poll: Poll) {
        let emoji: string = reaction.emoji.name;
        let userId: string = user.id;

        if (poll.pollData.pollOptions[emoji] === undefined) {
            await reaction.remove(userId);
            return;
        }

        if (poll.pollData.participants[userId] !== undefined) {
            if (!poll.pollData.multivote) {
                let oldReaction = reaction.message.reactions.find(element => element.emoji.name === poll.pollData.participants[userId]);
                oldReaction.remove(userId);
            }

            this.extensionData["polls"][poll.pollData.pollId] = poll.pollData;
            if (poll.pollData.multivote) return;
            poll.pollData.participants[userId] = emoji;
            //this.database.updateExtensionSettings(namespace, this.extensionData);
            //console.log(poll.pollData.participants);
        }

        if (poll.pollData.participants[userId] === undefined) {
            this.extensionData["polls"][poll.pollData.pollId] = poll.pollData;
            if (poll.pollData.multivote) return;
            poll.pollData.participants[userId] = emoji;
            //this.database.updateExtensionSettings(namespace, this.extensionData);
        }

        if (poll.pollData.multivote) return;

        let reactions = reaction.message.reactions.array();
        for (var i in reactions) {
            let aReaction = reactions[i];
            let hasUser = aReaction.users.find(element => element.id == userId);
            //console.log(hasUser);
            if ((aReaction.emoji.name !== emoji) && (hasUser !== null)) {
                //console.log("Removing " + aReaction.emoji.name);
                await aReaction.remove(userId);
            }
        }
    }

    async pollReactionRemove(reaction: Discord.MessageReaction, user: Discord.User, poll: Poll) {
        let emoji: string = reaction.emoji.name;
        let userId: string = user.id;

        if (poll.pollData.multivote) return;

        if (poll.pollData.pollOptions[emoji] === undefined) {
            return;
        }

        if (poll.pollData.participants[userId] !== undefined) {
            delete poll.pollData.participants[userId];
            return;
        }
    }
}

let numberEmoji: string[] = ["1⃣", "2⃣", "3⃣","4⃣","5⃣","6⃣","7⃣","8⃣","9⃣"];
class Poll {
    pollMessage?: Discord.Message;
    pollData: {pollId: string, 
                pollName: string,
                multivote: boolean,
                messageId: string,
                channelId: string,
                isPosted: boolean,
                pollOptions: {[emoji: string]: string},
                participants:{[userId: string]: string}};

    constructor(id: string, name: string, mVote?: boolean, pollOptions?: string[]) {
        this.pollData = {pollId: id, pollName: name, multivote: false, messageId: "", channelId: "", isPosted: false, pollOptions: {} as {[emoji:string]: string}, participants: {} as {[userId: string]: string}};
        if (pollOptions !== undefined) {
            this.pollData.pollOptions = this.generatePollOptions(pollOptions);
        }

        if (mVote !== undefined) {
            this.pollData.multivote = mVote;
        }
    }
    
    generatePollOptions(pollOptions: string[]) {
        var generatedOptions: {[emoji: string]: string} = {};

        if (pollOptions.length > 9) return {}; //Change later when functionality for larger polls is added

        for (var i in pollOptions) {
            let option = pollOptions[i];
            let emoji = numberEmoji[i];

            generatedOptions[emoji] = option;
        }

        return generatedOptions;
    }

    async postPoll(channel: Discord.TextChannel) {
        this.pollData.channelId = channel.id;
        let content: string = "";
        for (var option in this.pollData.pollOptions) {
            content = content + option + " " + this.pollData.pollOptions[option] + "\n";
        }

        let pollEmbed = createEmbed(content, {"title": this.pollData.pollName, "footer": "Vote by reacting with the corresponding reaction to this post.\nEach user only gets a single vote"});
        let message: Discord.Message = await channel.send(pollEmbed) as Discord.Message;

        if (message !== undefined) {
            this.pollData.messageId = message.id;
            this.pollData.isPosted = true;
            for (var option in this.pollData.pollOptions) {
                await message.react(option);
            }

            return true;
        }

        console.log("Something went wrong");
        return false;
    }
}

export {namespace}
export {events}
export {initExtension}