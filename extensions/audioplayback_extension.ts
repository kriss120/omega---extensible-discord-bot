import {Extension} from '../extension';
import {OmegaDB} from "../omegaDB";
import Discord = require('discord.js');
import {Guild} from "../guild";

const namespace: string = "audio";
const events: string[] = [];

const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {
    create: {
        commandUsage: "", 
        commandInfo: ""
    },
    play: {
        commandUsage: "",
        commandInfo: ""
    },
    delete: {
        commandUsage: "",
        commandInfo: ""
    }
}; 

function initExtension(guild: Guild, database: OmegaDB) {
    return new AudioPlayer(guild, database, commands);
}

class AudioPlayer extends Extension {
    extensionData: {[dataType: string]: any};
    commandList: {[commandName: string]: string};

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        super(guild, database, commands);
        this.extensionData = {};
        this.commandList = {};
    }

    async init() {
        let extensionDBStatus = await this.database.checkExtension(namespace);
        if (!extensionDBStatus) await this.database.generateExtensionSettings(namespace, {commandList: {}});

        this.extensionData = await this.database.getExtensionSettings(namespace);
        this.commandList = this.extensionData["commandList"];

        //Init dir structure for guild and extension!
    }

    listCommands() {
        return ["Example Extension"];
    }

    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        let command: string = cmd[0];
        let args: string[] = cmd.slice(1);
        
        switch (command) {
            case "play":
                this.playCommand(args, message);
                break;
            case "create":
                this.createCommand(args, message);
                break;
            case "delete":
                this.deleteCommand(args, message);
                break;
        }
    }

    //o!audio play <custom-audio-command-name>
    playCommand(args: string[], message: Discord.Message) {
        let audioCommand: string = args[0];
        if (!this.commandList[audioCommand]) return; //User feedback here please

        let audioSrc = this.commandList[audioCommand];
        this.playAudio(audioSrc, message);
    }

    //o!audio create <custom-command> <audio-file> 
    //Audio folder should be /audioplayback_extension/guildId/media/audiofile.mp3
    createCommand(args: string[], message: Discord.Message) {
        let audioCommand: string = args[0];
        let audioFile: string = args[1];
        if (this.commandList[audioCommand]) return; //User feedback here please

        this.commandList[audioCommand] = "./extensions/audioplayback_extension/" + this.guild.id + "/media/" + audioFile;

        this.extensionData["commandList"] = this.commandList;
        this.database.updateExtensionSettings(namespace, this.extensionData);
    }

    //o!audio delete <custom-command>
    deleteCommand(args: string[], message: Discord.Message) {
        let audioCommand: string = args[0];
        if (!this.commandList[audioCommand]) return; //User feedback here please

        delete this.commandList[audioCommand];

        this.extensionData["commandList"] = this.commandList;
        this.database.updateExtensionSettings(namespace, this.extensionData);
    }

    async playAudio(audioSrc: string, message: Discord.Message) {
        if (!message.guild) return; //Send error message

        if (message.member.voiceChannel) {
            let connection = await message.member.voiceChannel.join();
            const dispatcher = connection.playFile(audioSrc);
            dispatcher.setVolume(0.1);

            dispatcher.on('end', () => {
                connection.disconnect();
            });
        }
    }
}

export {namespace}
export {events}
export {initExtension}