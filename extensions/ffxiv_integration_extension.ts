import {OmegaDB} from "../omegaDB";
import Discord = require('discord.js');
import {Guild} from "../guild";
import {Extension} from "../extension";
import {createEmbed, checkFlags} from '../omegaToolkit';

//Modules and data required for FFXIV Integration
const XIVAPI = require('xivapi-js');
const ffxiv_auth = require('./ffxiv_integration_extension/ffxiv_auth.json');
const ffxivfont = 'OPTIEngeEtienne';

//Modules and data required for generating graphics
import request = require('request-promise');
import {createCanvas, loadImage, Image} from "canvas";

//Exported extension settings
const namespace: string = "ff"; //Declares the extensions namespace
const events: string[] = ["MESSAGE_CREATE", "MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE"]; //Declares what events the extension wants

//Commands and information about commands
const commands: {[command: string]: {commandUsage: string, commandInfo: string}} = {
    iam: {
        commandUsage: "`" + namespace + " iam <Forename> <Surname> <Server>`", 
        commandInfo: "Registers the FFXIV character with your discord user"
    },
    whoami: {
        commandUsage: "`" + namespace + " whoami`",
        commandInfo: "Returns information about your registered FFXIV character"
    },
    whois: {
        commandUsage: "`" + namespace + " whois @user` or `" + namespace + " whois <Forename> <Surname> <Server>`",
        commandInfo: "Returns information about the users registered FFXIV character or of the queried FFXIV character"
    },
    gear: {
        commandUsage: "`" + namespace + " gear` or `" + namespace + " gear @user` or `" + namespace + "gear <Forename> <Surname> <Server>`",
        commandInfo: "Returns information about the gear of the callers registered FFXIV character, the users FFXIV character or the queried FFXIV character"
    }
}; 

//Initializes a new extension class and returns it to the caller
function initExtension(guild: Guild, database: OmegaDB) {
    return new FFXIV_Integration(guild, database, commands);
}

//FFXIV Integration Extension
class FFXIV_Integration extends Extension {
    xiv: any;
    extensionData: {[dataType: string]: any};
    chrProfiles: {[discordId: string]: any};
    settings: {};

    constructor(guild: Guild, database: OmegaDB, commands?: {[command: string]: {commandUsage: string, commandInfo: string}}) {
        super(guild, database, commands);
        this.xiv = new XIVAPI(ffxiv_auth);
        this.extensionData = {};
        this.settings = {};
        this.chrProfiles = {};
    }

    async init() {
        //Used to init when there are database queries to wait for
        let extensionDBStatus = await this.database.checkExtension(namespace);
        if (!extensionDBStatus) {
            let extension_data = {
                settings: {},
                chrProfiles: {}
            }
            await this.database.generateExtensionSettings(namespace, extension_data);
        } 

        this.extensionData = await this.database.getExtensionSettings(namespace);
        this.settings = this.extensionData["settings"];
        this.chrProfiles = this.extensionData["chrProfiles"];
    }

    //Handles and resolves commands
    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
       let checkedCmds: {flags: string[], args: string[]} = checkFlags(cmd);
       let flags: string[] = checkedCmds.flags;
       let command: string = checkedCmds.args[0];
       let args = checkedCmds.args.slice(1);

       switch (command) {
            case "iam":
                this.iam(message, args, flags);
                break;
            case "whoami":
                this.whoAmI(message, args, flags);
                break;
            case "whois":
                this.whoIs(message, args, flags);
                break;
            case "gear":
                this.gear(message, args, flags);
            default:
                break;
       }
    }

    async resolveEvent(packet: any, type: string) {
        //Resolve events
        return;
    }

    //ob!ff iam <chrForename> <chrSurname> <serverName> 
    private async iam(message: Discord.Message, args: string[], flags: string[]) {
        let chrForename: string = args[0];
        let chrSurname: string = args[1];
        let chrServer: string = args[2];
        let chrProfile: any;
        
        if (chrForename === undefined || chrSurname === undefined) {
            console.log("No chrName");
            return;
        }
        if (chrServer === undefined || chrServer === null) {
            console.log("No chrServer");
            return;
        }

        message.channel.startTyping(); //Begin assembling response to command
        let searchObject: any;
        try {
            searchObject = await this.xiv.character.search(chrForename + " " + chrSurname, {server: chrServer});
        } catch {
            let embed = createEmbed("The request timed out, please try again.");
            message.channel.send(embed);
            message.channel.stopTyping();
            return;
        }
        let searchResult: any[] = searchObject["Results"];
        if (searchResult.length === 0) {
            let embed = createEmbed("There were no characters with the name **" + chrForename + "** **" + chrSurname + "** on **" + chrServer + "**");
            message.channel.send(embed);
            message.channel.stopTyping();
            return;
        }
        if (searchResult.length > 1) {
            let embed = createEmbed("Hmmm, strange. There were multiple results. Please try again.");
            message.channel.send(embed);
            message.channel.stopTyping();
            return;
        }

        if (searchResult.length === 1) chrProfile = searchResult[0];

        this.chrProfiles[message.member.user.id] = chrProfile;
        this.extensionData["chrProfiles"] = this.chrProfiles;
        this.database.updateExtensionSettings(namespace, this.extensionData);

        let content = "Your character profile has been registered!";
        let options = {
            title: chrProfile.Name,
            url: "https://na.finalfantasyxiv.com/lodestone/character/" + chrProfile.ID,
            thumbnail: chrProfile.Avatar
        }

        let embedReponse: Discord.RichEmbed = createEmbed(content, options);
        message.channel.send(embedReponse);
        message.channel.stopTyping(); //Finished assembling response to command
    }

    //ob!ff whoami 
    private async whoAmI(message: Discord.Message, args: string[], flags: string[]) {
        if (this.chrProfiles[message.member.user.id] === undefined) return; //Let caller know it failed, inform they need to do iam command

        let thisChrProfile = this.chrProfiles[message.member.user.id];

        message.channel.startTyping();
        let getProfile: any;
        try {
            getProfile = await this.xiv.character.get(thisChrProfile.ID, {extended: 1, data: "CJ"});
        } catch {
            let embed = createEmbed("The request timed out, please try again.");
            message.channel.send(embed);
            message.channel.stopTyping();
            return;
        }

        let chrProfile = getProfile.Character;

        let profileEmbed = await this.generateProfileEmbed(chrProfile);
        message.channel.send(profileEmbed);
        message.channel.stopTyping();

        //console.log(getProfile);
        //console.log(getProfile.Character.ClassJobs);
        //console.log(getProfile.Character.GearSet.Gear);
    }

    //ob!ff whois <chrForename>? <chrSurname>? <serverName>? @User?
    private async whoIs(message: Discord.Message, args: string[], flags: string[]) {
        let users: Discord.User[] = message.mentions.users.array();
        if (users.length !== 0) {
            message.channel.startTyping();
            for (var i in users) {
                if (this.chrProfiles[users[i].id] !== undefined) {
                    let thisChrSave: any = this.chrProfiles[users[i].id];
                    let getProfile: any;
                    try {
                        getProfile = await this.xiv.character.get(thisChrSave.ID, {extended: 1, data: "CJ"});
                    } catch {
                        let embed = createEmbed("The request timed out, please try again.");
                        message.channel.send(embed);
                        message.channel.stopTyping();
                        return;
                    }
                
                    let chrProfile: any = getProfile.Character;

                    let embedResponse = await this.generateProfileEmbed(chrProfile);
                    await message.channel.send(embedResponse);
                }
            }
            message.channel.stopTyping();
        } else {
            let chrForename: string = args[0];
            let chrSurname: string = args[1];
            let chrServer: string = args[2];

            if (chrForename === undefined || chrSurname === undefined) {
                console.log("No character name");
                return;
            }

            if (chrServer === undefined) {
                console.log("No character server");
                return;
            }

            message.channel.startTyping();
            let chrResult: any;
            
            let searchObject
            try {
                searchObject = await this.xiv.character.search(chrForename + " " + chrSurname, {server: chrServer});
            } catch {
                let embed = createEmbed("The request timed out, please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }

            let searchResult: any[] = searchObject["Results"];
            if (searchResult.length === 0) {
                let embed = createEmbed("There were no characters with the name **" + chrForename + "** **" + chrSurname + "** on **" + chrServer + "**");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }
            if (searchResult.length > 1) {
                let embed = createEmbed("Hmmm, strange. There were multiple results. Please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }

            if (searchResult.length === 1) chrResult = searchResult[0];
            let getProfile: any;
            try {
                getProfile = await this.xiv.character.get(chrResult.ID, {extended: 1, data: "CJ"});
            } catch {
                let embed = createEmbed("The request timed out, please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }
            
            let chrProfile: any = getProfile.Character;

            let embedResponse = await this.generateProfileEmbed(chrProfile);
            message.channel.send(embedResponse);
            message.channel.stopTyping();
        }
    }

    //ob!ff gear <chrForename>? <chrSurname>? <serverName>? @User?
    private async gear(message: Discord.Message, args: string[], flags: string[]) {
        let users: Discord.User[] = message.mentions.users.array();
        if (users.length !== 0) {
            message.channel.startTyping();
            for (var i in users) {
                if (this.chrProfiles[users[i].id] !== undefined) {
                    let thisChrSave: any = this.chrProfiles[users[i].id];
                    let getProfile: any;
                    try {
                        getProfile = await this.xiv.character.get(thisChrSave.ID, {extended: 1, data: "CJ"});
                    } catch {
                        let embed = createEmbed("The request timed out, please try again.");
                        message.channel.send(embed);
                        message.channel.stopTyping();
                        return;
                    }
                    
                    let chrProfile: any = getProfile.Character;

                    let embedResponse = await this.generateGearEmbed(chrProfile);
                    await message.channel.send(embedResponse);
                }
            }
            message.channel.stopTyping();
        } else if (args[0] !== undefined) {
            let chrForename: string = args[0];
            let chrSurname: string = args[1];
            let chrServer: string = args[2];

            if (chrForename === undefined || chrSurname === undefined) {
                console.log("No character name");
                return;
            }

            if (chrServer === undefined) {
                console.log("No character server");
                return;
            }

            message.channel.startTyping();
            let chrResult: any;
            let searchObject: any;
            try {
                searchObject = await this.xiv.character.search(chrForename + " " + chrSurname, {server: chrServer});
            } catch {
                let embed = createEmbed("The request timed out, please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }

            let searchResult: any[] = searchObject["Results"];
            if (searchResult.length === 0) {
                let embed = createEmbed("There were no characters with the name **" + chrForename + "** **" + chrSurname + "** on **" + chrServer + "**");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }
            if (searchResult.length > 1) {
                let embed = createEmbed("Hmmm, strange. There were multiple results. Please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }

            if (searchResult.length === 1) chrResult = searchResult[0];

            let getProfile: any;
            try {
                getProfile = await this.xiv.character.get(chrResult.ID, {extended: 1, data: "CJ"});
            } catch {
                let embed = createEmbed("The request timed out, please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }
    
            let chrProfile: any = getProfile.Character;

            let embedResponse = await this.generateGearEmbed(chrProfile);
            message.channel.send(embedResponse);
            message.channel.stopTyping();
        } else {
            if (this.chrProfiles[message.member.user.id] === undefined) return; //Let caller know it failed, inform they need to do iam command

            let thisChrProfile = this.chrProfiles[message.member.user.id];
    
            message.channel.startTyping();
            let getProfile: any;
            try {
                getProfile = await this.xiv.character.get(thisChrProfile.ID, {extended: 1, data: "CJ"});
            } catch {
                let embed = createEmbed("The request timed out, please try again.");
                message.channel.send(embed);
                message.channel.stopTyping();
                return;
            }

            let chrProfile = getProfile.Character;
    
            let profileEmbed = await this.generateGearEmbed(chrProfile);
            message.channel.send(profileEmbed);
            message.channel.stopTyping();
        }
    }

    //Creates custom image and returns it in an embed
    private async generateProfileEmbed(chrProfile: any) {
        //Implement pretty pretty image system
        var canvas = createCanvas(1280, 873);
        var ctx = canvas.getContext('2d');

        let img: Buffer = await this.getImage(chrProfile.Portrait);
        let profileImg = new Image();
        profileImg.src = img;

        let frame = await loadImage("./extensions/ffxiv_integration_extension/ffxiv_profile_frame.png");

        ctx.drawImage(profileImg, 640, 0);
        ctx.drawImage(frame, 0, 0);

        this.drawText(chrProfile.Name, 320, 100, ctx, {"font": '70px ' + ffxivfont, "alignment": "center"});
        this.drawText(chrProfile.Title.Name, 320, 150, ctx, {"font": '40px ' + ffxivfont, "alignment": "center"});

        this.drawLvlText(chrProfile.ClassJobs, ctx);

        let finalImage: Buffer = canvas.toBuffer();

        let content = "";
        let options =  {
            title: chrProfile.Name,
            url: "https://na.finalfantasyxiv.com/lodestone/character/" + chrProfile.ID,
            files: {attachment: finalImage, name: "profile.png"},
            image: "attachment://profile.png"
        }

        let profileEmbed:Discord.RichEmbed = createEmbed(content, options);
        return profileEmbed;
    }

    //Returns gearEmbed
    private async generateGearEmbed(chrProfile: any) {
        var canvas = createCanvas(1500, 920);
        var ctx = canvas.getContext('2d');
        let gear: any = chrProfile.GearSet;

        let img: Buffer = await this.getImage(chrProfile.Portrait);
        let profileImg = new Image();
        profileImg.src = img;

        let frame = await loadImage("./extensions/ffxiv_integration_extension/ffxiv_gear_frame.png");

        ctx.drawImage(profileImg, 450, 23.5);
        ctx.drawImage(frame, 0, 0);

        let jobTxt = "LVL " + chrProfile.ActiveClassJob.Level + " " + chrProfile.ActiveClassJob.Job.Name;
        this.drawText(jobTxt.toUpperCase(), 750, 700, ctx, {"font": '40px ' + ffxivfont, "alignment": "center"});
        this.drawText(chrProfile.Name, 750, 780, ctx, {"font": '70px ' + ffxivfont, "alignment": "center"});
        this.drawText(chrProfile.Title.Name, 750, 830, ctx, {"font": '40px ' + ffxivfont, "alignment": "center"});

        //Set default drawing coordinates and data
        let x = 350;  //Start X Pos
        let y = 20;   //Start Y pos
        let dx = 720; //Delta between rows
        let dim = 80; //Icon Dimensions
        let off = 50; //Drawing Offset
        let r = 1;    //Starting row
        let drawData: {[data: string]: number} = {"x": x, "y": y, "dx": dx, "dim": dim, "off": off, "r": r};

        await this.drawGearIcons(gear, ctx, drawData);
        await this.drawGearLvl(gear, ctx, drawData);
        await this.drawGearMelds(gear, ctx, drawData);
        await this.drawGearText(gear, ctx, drawData);

        let finalImage: Buffer = canvas.toBuffer();

        let content = "";
        let options =  {
            title: chrProfile.Name,
            url: "https://na.finalfantasyxiv.com/lodestone/character/" + chrProfile.ID,
            files: {attachment: finalImage, name: "gear.png"},
            image: "attachment://gear.png"
        }
        let profileEmbed: Discord.RichEmbed = createEmbed(content, options);
        return profileEmbed;
    }

    //Get image data and return as a buffer
    private async getImage(URL: string) {
        let requestSettings = {
            url: URL,
            method: 'GET',
            encoding: null
        };
        
        let image = await request(requestSettings);
        return image;
    }

    //Draw Generic Text
    private drawText(text: string, x: number, y: number, ctx: any, options?: {[option: string]: any}) {
        var alignment: string = "left";
        var color: string = "rgb(255, 255, 255)";
        var font: string = "30px Liberation Serif";
        var x: number = x;
        var y: number = y;

        //Check for custom options
        for (var option in options) {
            switch (option) {
                case "alignment":
                    alignment = options[option];
                    break;
                case "font":
                    font = options[option];
                    break;
                case "color":
                    color = options[option];
                    break;
                default:
                    break;
            }
        }

        //Set draw styles
        ctx.font = font;
        ctx.fillStyle = color;

        //Calculate alignment
        let textData = ctx.measureText(text); //Measure text length
        switch (alignment) {
            case "center":
                let halfWidth = textData.width / 2;
                x = x - halfWidth;
                break;
            case "right":
                x = x - textData.width;
                break;
            default:
                break;
        }

        ctx.fillText(text, x , y);
    }

    //Draw lvl text on image
    private drawLvlText(chrClassJobs: any, ctx: any) {
        let jobs: {[abr: string]: string} = {
            "MRD": "-",
            "DRK": "-",
            "GLA": "-",
            "GNB": "-",
            "CNJ": "-",
            "SCH": "-",
            "AST": "-",
            "PGL": "-",
            "LNC": "-",
            "ROG": "-",
            "SAM": "-",
            "ARC": "-",
            "MCH": "-",
            "DNC": "-",
            "THM": "-",
            "SMN": "-",
            "RDM": "-",
            "BLU": "-",
            "MIN": "-",
            "BTN": "-",
            "FSH": "-",
            "CRP": "-",
            "BSM": "-",
            "ARM": "-",
            "GSM": "-",
            "LTW": "-",
            "WVR": "-",
            "ALC": "-",
            "CUL": "-"
        }

        for (var i in chrClassJobs) {
            let thisChrClassJob = chrClassJobs[i];
            let classJob: string = thisChrClassJob.Class.Abbreviation;

            if (classJob === "ACN") {
                jobs["SCH"] = thisChrClassJob.Level;
                jobs["SMN"] = thisChrClassJob.Level;
            } else {
                jobs[classJob] = thisChrClassJob.Level;
            }
        }

        ctx.font = '41.25px ' + ffxivfont;
        
        
        var x = 122.5;
        var y = 284.875;
        var r = 1;

        for (var job in jobs) {
            if (y > 722.375) {
                y = 284.875;
                x = x + 150;
                r = r + 1;
            }
            
            if (y == 534.875 && r != 4) y = y + 62.5;

            ctx.fillText(jobs[job], x, y);
            y = y + 62.5;
        }


    }

    //GEAR DRAWING METHODS
    //Draw Gear Icons
    private async drawGearIcons(gear: any, ctx: any, drawData: {[data: string]: number}) {
        let items: {[item: string]: string} = {
            "MainHand": "",
            "Head": "",
            "Body": "",
            "Hands": "",
            "Waist": "",
            "Legs": "",
            "Feet": "",
            "OffHand": "",
            "Earrings": "",
            "Necklace": "",
            "Bracelets": "",
            "Ring1": "",
            "Ring2": ""
        }

        //Get gear icon urls
        let gearItems = gear.Gear;
        for (var item in gearItems) {
            if (items[item] !== undefined) {
                items[item] = "https://xivapi.com" + gearItems[item].Item.Icon;
            }
        }

        //Download gear icons to buffer
        let itemImgs: {[itemName: string]: Buffer} = {};
        for (var item in items) {
            if (items[item] !== "") {
                let img: Buffer = await this.getImage(items[item]);
                itemImgs[item] = img;
            } else {
                let imgLoad = await loadImage("./extensions/ffxiv_integration_extension/empty_icon.png");
                let img: Buffer = imgLoad.src as Buffer;
                itemImgs[item] = img;
            }
        }

        //Draw gear icons from buffer
        var x = drawData.x;
        var y = drawData.y;
        var dim = drawData.dim;
        var off = drawData.off;
        var r = drawData.r;
        for (var item in itemImgs) {
            let icon = new Image();
            icon.src = itemImgs[item];

            if (y > (drawData.y + 6*(dim + off)) && r === 1) {
                r = 2;
                y = drawData.y;
                x = drawData.x + drawData.dx;
            }

            ctx.drawImage(icon, x, y, dim, dim);

            y = y + dim + off;
        }
    }
    //Draw Gear Lvl
    private async drawGearLvl(gear: any, ctx: any, drawData: {[data: string]: number}) {
        let items: {[item: string]: string} = {
            "MainHand": "",
            "Head": "",
            "Body": "",
            "Hands": "",
            "Waist": "",
            "Legs": "",
            "Feet": "",
            "OffHand": "",
            "Earrings": "",
            "Necklace": "",
            "Bracelets": "",
            "Ring1": "",
            "Ring2": ""
        }

        //Get gear lvls
        let gearItems = gear.Gear;
        for (var item in gearItems) {
            if (items[item] !== undefined) {
                items[item] = gearItems[item].Item.LevelItem;
            }
        }

        //Initialize coordinates and offsets
        let textOffX = 10;
        let textOffY = 45;
        var x = drawData.x - textOffX;
        var y = drawData.y + textOffY;
        var r = drawData.r;
        var dim = drawData.dim;
        var off = drawData.off;
        var alignment = "right";

        //Draw Lvl Text
        for (var item in items) {
            if (y > (drawData.y + textOffY + 6*(dim + off)) && r === 1) {
                r = 2;
                y = drawData.y + textOffY;
                x = drawData.x + drawData.dx + drawData.dim + textOffX;
                alignment = "left";
            }

            this.drawText(items[item], x, y, ctx, {"alignment": alignment, "font": "50px " + ffxivfont, "color": "orange"});

            y = y + dim + off;
        }

    }
    //Draw Gear Melds
    private async drawGearMelds(gear: any, ctx: any, drawData: {[data: string]: number}) {
        let items: {[item: string]: any} = {
            "MainHand": [],
            "Head": [],
            "Body": [],
            "Hands": [],
            "Waist": [],
            "Legs": [],
            "Feet": [],
            "OffHand": [],
            "Earrings": [],
            "Necklace": [],
            "Bracelets": [],
            "Ring1": [],
            "Ring2": []
        }

        //Get gear melds
        let gearItems = gear.Gear;
        for (var item in gearItems) {
            if (items[item] !== undefined) {
                items[item] = gearItems[item].Materia;
            }
        }
        
        //Get Meld Icons
        var meldIcons: {[iconUrl: string]: Buffer} = {};
        for (var item in items) {
            let melds = items[item];
            for (var i in melds) {
                let materia = melds[i];
                if (meldIcons[materia.ID] === undefined) {
                    let img: Buffer = await this.getImage("https://xivapi.com" + materia.Icon);
                    meldIcons[materia.ID] = img;
                }
            }
        }

        //Initialize draw coordinates and offsets
        let offsetX = 125;
        let offsetY = 0;
        let iconDim = 50;
        let iconDx = 10;
        var x = drawData.x - offsetX;
        var y = drawData.y + offsetY;
        var r = drawData.r;
        var startX = drawData.x - offsetX;
        var dim = drawData.dim;
        var off = drawData.off;

        //Draw Meld Icons
        for (var item in items) {
            if (y > (drawData.y + offsetY + 6*(dim + off)) && r === 1) {
                r = 2;
                y = drawData.y + offsetY;
                x = drawData.x + drawData.dx + drawData.dim + offsetX - iconDim;
                startX = x;
            }

            let melds = items[item];
            for (var i in melds) {
                let materia = melds[i];
                let icon = new Image();
                icon.src = meldIcons[materia.ID];

                ctx.drawImage(icon, x, y, iconDim, iconDim);
                if (r === 1) {
                    x = x - iconDim - iconDx;
                } else if (r === 2) {
                    x = x + iconDim + iconDx;
                }
            }
            
            x = startX;
            y = y + dim + off;
        }
    }
    //Draw Gear Text
    private async drawGearText(gear: any, ctx: any, drawData: {[data: string]: number}) {
        let items: {[item: string]: {[type: string]: string}} = {
            "MainHand": {},
            "Head": {},
            "Body": {},
            "Hands": {},
            "Waist": {},
            "Legs": {},
            "Feet": {},
            "OffHand": {},
            "Earrings": {},
            "Necklace": {},
            "Bracelets": {},
            "Ring1": {},
            "Ring2": {}
        }

        //Get gear and mirage names
        let gearItems = gear.Gear;
        for (var item in gearItems) {
            if (items[item] !== undefined) {
                let mirage = "";
                if (gearItems[item].Mirage !== null) mirage = gearItems[item].Mirage.Name;
                items[item] = {"name": gearItems[item].Item.Name, "mirage": mirage}; 
            } 
        }

        //Init coordinates and offsets 
        let textOffX = 10;
        let textOffY = 80;
        var x = drawData.x - textOffX;
        var y = drawData.y + textOffY;
        var r = drawData.r;
        var dim = drawData.dim;
        var off = drawData.off;
        var alignment = "right";

        //Draw gear text
        for (var item in items) {
            if (y > (drawData.y + textOffY + 6*(dim + off)) && r === 1) {
                r = 2;
                y = drawData.y + textOffY;
                x = drawData.x + drawData.dx + drawData.dim + textOffX;
                alignment = "left";
            }

            if (items[item].name !== undefined) this.drawText(items[item].name, x, y, ctx, {"alignment": alignment, "font": "30px " + ffxivfont, "color": "white"});
            if (items[item].mirage !== undefined) this.drawText(items[item].mirage, x, y + 35, ctx, {"alignment": alignment, "font": "30px " + ffxivfont, "color": "orange"});

            y = y + dim + off;
        }

    }

    //LOGS AND PARSES TOXIC COMMANDS COMES HERE!
}

export {namespace}
export {events}
export {initExtension}
export {Extension}

