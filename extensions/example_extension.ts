import {Extension} from '../extension';
import {OmegaDB} from "../omegaDB";
import Discord = require('discord.js');
import {Guild} from "../guild";

const namespace: string = "example_extension";
const commands: string[] = ["hello_world"];
const events: string[] = ["MESSAGE_CREATE", "MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE"];

function initExtension(guild: Guild, database: OmegaDB) {
    return new Example_Extension(guild, database, commands);
}

class Example_Extension extends Extension {
    constructor(guild: Guild, database: OmegaDB, commands?: string[]) {
        super(guild, database, commands);
    }

    async init() {

    }

    listCommands() {
        return ["Example Extension"];
    }

    eventOnCommandMessage(cmd: string[], message: Discord.Message) {
        let args: string[] = cmd.slice(1);
        if (cmd[0] === "hello_world") {
            console.log(args);
        }
    }

    async resolveEvent(packet: any, type: string) {
        let eventType = type;

        switch (eventType) {
            case "MESSAGE_CREATE":
                this.eventOnMessage(packet["message"]);
                break;
            case "MESSAGE_REACTION_ADD":
                this.eventOnMessageReactionAdd(packet);
                break;
            default:
                console.log(eventType);
                break;
        }

        return;
    }

    eventOnMessage(message: Discord.Message) {
        //console.log(message.content);
        return;
    }

    eventOnMessageReactionAdd(packet: any) {
        let messageReaction: Discord.MessageReaction = packet["messageReaction"];
        let user: Discord.User = packet["user"];

        console.log(user.username);
        console.log(messageReaction.emoji.name);

        console.log("MessageReactionAddEvent Handeled");
        return;
    }

    eventOnMessageReactionRemove(packet: any) {
        console.log("MessageReactionRemoveEvent Handeled");
        return;
    }

    eventOnRaw() {
        return;
    }
}

export {namespace}
export {events}
export {initExtension}